package id.soevar.parkir;

import id.soevar.parkir.controller.Menu;

/**
 *
 * @author Bang Pardi
 */
public class App {

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.getMenuAwal();
    }
}
