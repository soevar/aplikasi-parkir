package id.soevar.parkir.controller;

import id.soevar.parkir.model.Info;
import java.util.Scanner;

/**
 *
 * @author Bang Pardi
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu Masuk Parkir");
        System.out.println("2. Menu Keluar Parkir");
        System.out.println("3. Laporan Harian");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        ParkirController pc = new ParkirController();
        switch (noMenu) {
            case 1:
                pc.setMasukParkir();
                break;
            case 2:
                pc.setKeluarParkir();
                break;
            case 3:
                pc.getDataParkir();
                break;
            case 4:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}
