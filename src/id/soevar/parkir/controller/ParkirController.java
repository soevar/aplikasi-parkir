package id.soevar.parkir.controller;

import com.google.gson.Gson;
import id.soevar.parkir.model.Parkir;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Bang Pardi
 */
public class ParkirController {

    private static final String FILE = "E:\\opt\\parkir.json";
    private Parkir parkir;
    private final Scanner in;
    private String noPlat;
    private int jenisKendaraan;
    private final LocalDateTime waktuMasuk;
    private LocalDateTime waktuKeluar;
    private BigDecimal biaya;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;

    public ParkirController() {
        in = new Scanner(System.in);
        waktuMasuk = LocalDateTime.now();
        waktuKeluar = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    /**
     * Method untuk menginput data masuk parkir
     */
    public void setMasukParkir() {
        System.out.print("Masukkan NoPol : ");
        noPlat = in.next();

        String formatWaktuMasuk = waktuMasuk.format(dateTimeFormat);
        System.out.println("Waktu Masuk : " + formatWaktuMasuk);

        System.out.println("Jenis Kendaraan 1=Motor, 2=Mobil");
        System.out.print("Masukkan Jenis Kendaran : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Masukkan Jenis Kendaran : ");
        }
        jenisKendaraan = in.nextInt();

        parkir = new Parkir();
        parkir.setNoPlat(noPlat.toUpperCase());
        parkir.setWaktuMasuk(waktuMasuk);
        parkir.setJenis(jenisKendaraan);

        setWriteParkir(FILE, parkir);

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setMasukParkir();
        }
    }

    /**
     * Method untuk menyimpan data parkir ke file berbentuk json
     *
     * @param file, tipedata <code>String</code> merujuk ke file yang akan
     * disimpan.
     * @param parkir, model <code>Parkir</code> yang akan disimpan.
     */
    public void setWriteParkir(String file, Parkir parkir) {
        Gson gson = new Gson();

        List<Parkir> parkirs = getReadParkir(file);
        parkirs.remove(parkir);
        parkirs.add(parkir);

        String json = gson.toJson(parkirs);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ParkirController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method untuk menginput data keluar parkir
     */
    public void setKeluarParkir() {
        System.out.print("Masukkan NoPol : ");
        noPlat = in.next();

        Parkir p = getSearch(noPlat);
        if (p != null) {
            LocalDateTime tempWaktu = LocalDateTime.from(p.getWaktuMasuk());
            waktuKeluar = LocalDateTime.now();
            long jam = tempWaktu.until(waktuKeluar, ChronoUnit.HOURS);
            p.setWaktuKeluar(waktuKeluar);

            if (jam > 0) {
                biaya = new BigDecimal(jam);// biaya.add(BigDecimal.valueOf(jam))..add();
                if (p.getJenis() == 1) {
                    biaya = biaya.multiply(new BigDecimal(1000));
                } else {
                    biaya = biaya.multiply(new BigDecimal(2000));
                }
            } else {
                jam = 1;
                if (p.getJenis() == 1) {
                    biaya = new BigDecimal(1000);
                } else {
                    biaya = new BigDecimal(2000);
                }
            }
            p.setBiaya(biaya);
            p.setKeluar(true);

            System.out.println("No Polisi : " + p.getNoPlat());
            System.out.println("Jenis : " + (p.getJenis() == 1 ? "Motor" : "Mobil"));
            System.out.println("Waktu Masuk : " + p.getWaktuMasuk().format(dateTimeFormat));
            System.out.println("Waktu Keluar : " + p.getWaktuKeluar().format(dateTimeFormat));
            System.out.println("Lama : " + jam + " Jam");
            System.out.println("Biaya : " + biaya);

            System.out.println("Proses Bayar?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    setWriteParkir(FILE, p);
                    break;
                case 2:
                    setKeluarParkir();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Apakah mau memproses kembali?");
            System.out.print("1) Ya, 2) Tidak : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setKeluarParkir();
            }
        } else {
            System.out.println("Data tidak ditemukan");
            setKeluarParkir();
        }
    }

    /**
     * Method untuk mencari nomor polisi dari data file json
     *
     * @param noPol, tipedata <code>String</code> merujuk ke data yang akan
     * dicari.
     * @return <code>Parkir</code>
     */
    public Parkir getSearch(String noPol) {
        List<Parkir> parkirs = getReadParkir(FILE);// Arrays.asList(parkir);

        Parkir p = parkirs.stream()
                .filter(pp -> noPol.equalsIgnoreCase(pp.getNoPlat()))
                .findAny()
                .orElse(null);

        return p;
    }

    /**
     * Method untuk mencari nomor polisi dari data file json
     *
     * @param file, tipedata <code>String</code> merujuk ke file penyimpanan
     * data.
     * @return <code>List<Parkir></code>
     */
    public List<Parkir> getReadParkir(String file) {
        List<Parkir> parkirs = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try ( Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Parkir[] ps = gson.fromJson(line, Parkir[].class);
                parkirs.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ParkirController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ParkirController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return parkirs;
    }

    /**
     * Method untuk menampilkan data parkir yang sudah keluar
     */
    public void getDataParkir() {
        List<Parkir> parkirs = getReadParkir(FILE);
        Predicate<Parkir> isKeluar = e -> e.isKeluar() == true;
        Predicate<Parkir> isDate = e -> e.getWaktuKeluar().toLocalDate().equals(LocalDate.now());

        List<Parkir> pResults = parkirs.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = pResults.stream()
                .map(Parkir::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("No Polisi \tWaktu Masuk \t\tWaktu Keluar \t\tBiaya");
        System.out.println("--------- \t----------- \t\t------------ \t\t-----");
        pResults.forEach((p) -> {
            System.out.println(p.getNoPlat() + "\t\t" + p.getWaktuMasuk().format(dateTimeFormat) + "\t" + p.getWaktuKeluar().format(dateTimeFormat) + "\t" + p.getBiaya());
        });
        System.out.println("--------- \t----------- \t\t------------ \t\t-----");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataParkir();
        }
    }
}
