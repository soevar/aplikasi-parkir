package id.soevar.parkir.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author Bang Pardi
 */
public class Parkir implements Serializable {

    private static final long serialVersionUID = -6756463875294313469L;

    private String noPlat;
    private int jenis;
    private LocalDateTime waktuMasuk;
    private LocalDateTime waktuKeluar;
    private BigDecimal biaya;
    private boolean keluar = false;

    public Parkir() {

    }

    public Parkir(String noPlat, int jenis, LocalDateTime waktuMasuk, LocalDateTime waktuKeluar, BigDecimal biaya) {
        this.noPlat = noPlat;
        this.jenis = jenis;
        this.waktuMasuk = waktuMasuk;
        this.waktuKeluar = waktuKeluar;
        this.biaya = biaya;
    }

    public String getNoPlat() {
        return noPlat;
    }

    public void setNoPlat(String noPlat) {
        this.noPlat = noPlat;
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public LocalDateTime getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(LocalDateTime waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public LocalDateTime getWaktuKeluar() {
        return waktuKeluar;
    }

    public void setWaktuKeluar(LocalDateTime waktuKeluar) {
        this.waktuKeluar = waktuKeluar;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }

    @Override
    public String toString() {
        return "Parkir{" + "noPlat=" + noPlat + ", jenis=" + jenis + ", waktuMasuk=" + waktuMasuk + ", waktuKeluar=" + waktuKeluar + ", biaya=" + biaya + ", keluar=" + keluar + '}';
    }
}
