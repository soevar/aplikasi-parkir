package id.soevar.parkir.model;

/**
 *
 * @author Bang Pardi
 */
public class Info {

    private final String aplikasi = "Aplikasi Parkir Sederhana";
    private final String version = "Versi 1.0.0";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;
    }
}
