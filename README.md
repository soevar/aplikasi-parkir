# Aplikasi Parkir #
Repository ini berisi Contoh Aplikasi Parkir Sederhana menggunakan Java yang berbentuk CLI (Command Line Interface)

## Konfigurasi ##
* Ubah lokasi `FILE` pada class `ParkirController`
* Contoh file `parkir.json` berada pada folder `lib`
* Tambahkan library `gson-2.8.6.jar`, file tersedia di folder `lib`

## Cara Menjalankan dan Kompilasi ##
* Pilih menu `Run` -> `Clean and Build Project` atau bisa langsung `Shift + F11`
* Terbuat folder `dist`, masuk ke folder tersebut
* Ketik perintah `java -jar AplikasiParkir.jar`

### Class Diagram ###
![Class Diagram](img/class-diagram.png "Class Diagram Aplikasi Parkir")